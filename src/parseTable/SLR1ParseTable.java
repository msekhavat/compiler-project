package parseTable;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import cfg.Token;

public class SLR1ParseTable {
	private ArrayList<Token> tokens;
	private int stateNum;
	private HashMap<Token, Integer> tokenId;
	private PTB[][] table;
	
	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}
	
	public SLR1ParseTable(ArrayList<Token> tokens, int stateNum){
		this.tokens = tokens;
		this.stateNum = stateNum;
		table = new PTB[stateNum][tokens.size()];
		initTokenIds();
	}

	private void initTokenIds() {
		tokenId = new HashMap<Token, Integer>();
		int i = 0;
		for(Token t : tokens){
			tokenId.put(t, i++);
		}
	}
	
	public void setPTB(int row, Token t, PTB ptb){
		Integer col = tokenId.get(t);
		if(col == null){
			throw new NullPointerException();
		}
		table[row][col] = ptb;
	}
	
	public PTB getPTB(int row, Token t){
		Integer col = tokenId.get(t);
		if(col == null || !inTable(row, col)){
			return null;
		}
		return table[row][col];
	}
	
	private boolean inTable(int row, int col){
		return (row >= 0 && row < stateNum) && (col >= 0 && col < tokens.size());
	}
	
	public Action getAction(int row, Token t){
		Integer col = tokenId.get(t);
		if(col == null || !inTable(row, col)){
			return null;
		}
		return table[row][col].getAction();
	}
	
	public int getNum(int row, Token t){
		Integer col = tokenId.get(t);
		if(col == null || !inTable(row, col)){
			return -1;
		}
		return table[row][col].getNum();
	}
	
	public boolean isEmpty(int row, Token t){
		Integer col = tokenId.get(t);
		if(col == null || !inTable(row, col)){
			throw new RuntimeException();
		}
		return (table[row][col] == null || table[row][col].getAction() == Action.ERROR);
	}

	public void print() {
		for(int i = 0 ; i < tokens.size() ; i++){
			System.out.print(tokens.get(i).getName() + "\t\t\t\t");
		}
		System.out.println();
		for(int i = 0 ; i < stateNum ; i++){
			PTB[] row = table[i];
			for(int j = 0 ; j < row.length ; j++){
				System.out.print(table[i][j] + "\t");
			}
			System.out.println();
		}
	}
	
	public void print(String fileName){
		try {
			PrintWriter out = new PrintWriter(fileName);
			for(int i = 0 ; i < tokens.size() ; i++){
				out.print(tokens.get(i).getName() + " ");
			}
			out.println();
			for(int i = 0 ; i < stateNum ; i++){
				PTB[] row = table[i];
				for(int j = 0 ; j < row.length ; j++){
					out.print(table[i][j] + "\t");
				}
				out.println();
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
