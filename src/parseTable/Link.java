package parseTable;

import cfg.Token;

public class Link {
	private Token token;
	private int state;
	
	public Link(Token token, int state){
		this.token = token;
		this.state = state;
	}

	public Token getToken() {
		return token;
	}

	public int getState() {
		return state;
	}

	@Override
	public String toString() {
		return "Link [token=" + token + ", state=" + state + "]";
	}
	
	
}
