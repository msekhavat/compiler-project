package parseTable;

import java.util.ArrayList;
import java.util.Stack;

import cfg.CFG;
import cfg.Rule;
import cfg.Token;
import cfg.TokenType;

public class ParseTableGenerator {
	private CFG grammar;
	private ArrayList<State> states;
	private int acceptState = -1;

	public CFG getGrammar() {
		return grammar;
	}

	public void setGrammar(CFG grammar) {
		this.grammar = grammar;
	}

	public ParseTableGenerator(CFG grammar){
		this.grammar = grammar;
		states = new ArrayList<State>();
	}

	public SLR1ParseTable SLR1ParseTable(){
		states = new ArrayList<State>();
		ArrayList<ParseRule> start = new ArrayList<ParseRule>();
		start.add(new ParseRule(grammar.getRule(0), 0));
		states.add(genState(0, start));
		generateAutomaton(0);
		return generateParseTable();
	}
	

	private SLR1ParseTable generateParseTable() {
		SLR1ParseTable table = new SLR1ParseTable(grammar.getTokens(), states.size());
		for(int i = 0 ; i < states.size() ; i++){
			State s = states.get(i);
			int num = s.getNumber();
			//Reduces
			for(ParseRule r : s.getRules()){
				if(r.getRule().getSize() == r.getDot()){
					Token LHS = r.getRule().getLHS();
					if(LHS.getType() == TokenType.START){
						continue;
					}
					ArrayList<Token> tokens = grammar.follow(LHS);
					for(Token t : tokens){
						if(!table.isEmpty(num,t )){
							throw new RuntimeException("Not SLR1, Conflict in State " + num + " and token " + t + "\n" + "In the table : " + 
											table.getPTB(num, t) + "\n" + "We want to push " + new PTB(Action.REDUCE, r.getRule().getNum())
											+ "\n\n" + s);
						}
						table.setPTB(num, t, new PTB(Action.REDUCE, r.getRule().getNum()));
					}
				}
			}
			
			//Shift and Goto
			for(Link li : s.getNeighbors()){
				Token t = li.getToken();
				if(!table.isEmpty(num,t )){
					throw new RuntimeException("Not SLR1, Conflict in State " + num + " and token " + t + "\n" + "In the table : " + 
							table.getPTB(num, t) + "\n" + "We want to push either shift, goto, or accept \n\n" + s );
				}
				Action act;
				if(li.getState() == acceptState){
					act = Action.ACCEPT;
				}
				else{
					act = (t.getType() == TokenType.VAR ? Action.GOTO : Action.SHIFT);
				}
				table.setPTB(num, t, new PTB(act, li.getState()));
			}
			
			for(Token t : grammar.getTokens()){
				if(table.isEmpty(num, t)){
					table.setPTB(num, t, new PTB(Action.ERROR, 0));
				}
			}
		}
		return table;
	}

	private void generateAutomaton(int i) {
		ArrayList<Token> seenTokens = new ArrayList<Token>();
		State state = states.get(i);
		for(ParseRule r : state.getRules()){
			int dot = r.getDot();
			if(dot == r.getRule().getSize()){
				continue;
			}
			Token t = r.getRule().getToken(dot);
			if(seenTokens.contains(t)){
				continue;
			}
			seenTokens.add(t);
			ArrayList<ParseRule> newKernel = new ArrayList<ParseRule>();
			for(ParseRule rule : state.getTokenRules(t)){
				ParseRule nRule = rule.getClone();
				nRule.incDot();
				newKernel.add(nRule);
			}
			State newState = genState(states.size(), newKernel);
			int tmpIndex = states.indexOf(newState);
			if(tmpIndex == -1){
				states.add(newState);
				
				//Check for acceptState
				if(newState.getKernel().size() == 1){
					ParseRule pRule = newState.getKernel().get(0);
					if(pRule.getRule().getNum() == 0 && pRule.getDot() == pRule.getRule().getSize()){
						acceptState = newState.getNumber();
					}
				}
				state.addLink(new Link(t, newState.getNumber()));
				generateAutomaton(newState.getNumber());
			}
			else{
				state.addLink(new Link(t, tmpIndex));
			}
		}
	}
	
	
	public State genState(int num, ArrayList<ParseRule> kernel){
		if(kernel == null){
			throw new NullPointerException();
		}
		State result = new State(num, kernel);
		Stack<Token> unChecked = new Stack<Token>();
		ArrayList<Token> checked = new ArrayList<Token>();
		for(ParseRule r : kernel){
			int dot = r.getDot();
			if(dot == r.getRule().getSize()){
				continue;
			}
			Token t = r.getRule().getToken(dot);
			if(!unChecked.contains(t)){
				unChecked.add(t);
			}
		}
		
		while(!unChecked.isEmpty()){
			Token t = unChecked.pop();
			checked.add(t);
			for(Rule rule : grammar.getTokenRules(t)){
				result.addNonKernel(new ParseRule(rule, 0));
				if(rule.getSize() > 0){
					Token first = rule.getRHS().get(0);
					if(!unChecked.contains(first) && !checked.contains(first)){
						unChecked.push(first);
					}
				}
			}
		}
		
	/*	for(ParseRule r : kernel){
			int dot = r.getDot();
			if(dot == r.getRule().getSize()){
				continue;
			}
			Token t = r.getRule().getToken(dot);
		}*/
		return result;
	}

	@Override
	public String toString() {
		String r = "";
		for(State s : states){
			r += s.toString() + "\n";
		}
		return r;
	}
	
	
}
