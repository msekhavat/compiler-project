package parseTable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


import cfg.*;

public class GetParseTable {
	
	public static ParseTableGenerator getParseTable(String args) throws FileNotFoundException{
		String inputFile = args;
		Token start;
		ArrayList<Rule> rules = new ArrayList<Rule>();
		Scanner s = new Scanner(new FileInputStream(inputFile));
		start = new Token(s.next().trim(), TokenType.VAR);
		s.nextLine();
		int i = 1;
		while(s.hasNext()){
			String r = s.nextLine();
			Scanner lineS = new Scanner(r);
			Token LHS = new Token(lineS.next().trim(), TokenType.VAR);
			if(!lineS.next().trim().equals(":")){
				System.out.println("Format Error");
				lineS.close();
				throw new RuntimeException();
			}
			ArrayList<Token> RHS = new ArrayList<Token>();
			while(lineS.hasNext()){
				String name = lineS.next();
				Token t = new Token((name.startsWith("#") ? name.substring(1) : name), 
						(name.startsWith("#") ? TokenType.VAR : TokenType.TERMINAL));
				RHS.add(t);
			}
			rules.add(new Rule(LHS, RHS, i++));
			lineS.close();
		}
		CFG g = new CFG(start, rules);
		ParseTableGenerator pg = new ParseTableGenerator(g);
		pg.SLR1ParseTable().print("output.txt");//FIXME delete this line
		s.close();
		return pg;
	}
	
}
