package parseTable;

import cfg.Rule;

public class ParseRule {
	private Rule rule;
	private int dot;
	
	public ParseRule(Rule rule, int dot){
		this.rule = rule;
		this.dot = dot;
	}

	public Rule getRule() {
		return rule;
	}

	public int getDot() {
		return dot;
	}
	
	public void incDot(){
		dot++;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dot;
		result = prime * result + ((rule == null) ? 0 : rule.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParseRule other = (ParseRule) obj;
		if (dot != other.dot)
			return false;
		if (rule == null) {
			if (other.rule != null)
				return false;
		} else if (!rule.equals(other.rule))
			return false;
		return true;
	}
	
	public ParseRule getClone(){
		ParseRule p = new ParseRule(rule, dot);
		return p;
	}

	@Override
	public String toString() {
		String result = rule.getLHS() + " -> ";
		if(rule.getSize() == 0){
			return result;
		}
		for(int i = 0 ; i < dot ; i++){
			result += rule.getRHS().get(i) + " ";
		}
		result += ". ";
		for(int i = dot ; i < rule.getSize() ; i++){
			result += rule.getRHS().get(i) + " ";
		}
		return result;
	}
	
	
}
