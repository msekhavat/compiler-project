package parseTable;

public class PTB {
	private Action action;
	private int num;
	
	public PTB(Action action, int num){
		this.action = action;
		this.num = num;
	}

	public Action getAction() {
		return action;
	}

	public int getNum() {
		return num;
	}

	@Override
	public String toString() {
		return "[" + action + "," + num + "]";
	}
	
}
