package parseTable;

import java.util.ArrayList;
import java.util.HashMap;

import cfg.Token;

public class State {
	private ArrayList<ParseRule> kernel;
	private ArrayList<ParseRule> nonKernel;
	private int number;
	private HashMap<Token, ArrayList<ParseRule>> tokenRules;
	
	private ArrayList<Link> neighbors;
	
	public State(int number, ArrayList<ParseRule> kernel){
		this.kernel = kernel;
		nonKernel = new ArrayList<ParseRule>();
		this.number = number;
		neighbors = new ArrayList<Link>();
		tokenRules = new HashMap<Token, ArrayList<ParseRule>>();
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kernel == null) ? 0 : kernel.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (kernel == null) {
			if (other.kernel != null)
				return false;
		} else if (!kernel.equals(other.kernel))
			return false;
		return true;
	}


	public boolean contains(ParseRule rule){
		for(ParseRule r : kernel){
			if(r.equals(rule)){
				return true;
			}
		}
		for(ParseRule r : nonKernel){
			if(r.equals(nonKernel)){
				return true;
			}
		}
		return false;
	}
	
	public void addNonKernel(ParseRule rule){
		if(!contains(rule)){
			nonKernel.add(rule);
		}
	}
	
	public ArrayList<ParseRule> getRules(){
		ArrayList<ParseRule> result = new ArrayList<ParseRule>(kernel);
		result.addAll(nonKernel);
		return result;
	}

	public ArrayList<ParseRule> getTokenRules(Token t){
		ArrayList<ParseRule> result = new ArrayList<ParseRule>();
		for(ParseRule r : getRules()){
			int dot = r.getDot();
			if(r.getRule().getSize() == dot){
				continue;
			}
			if(r.getRule().getToken(dot).equals((t))){
				result.add(r);
			}
		}
		return result;
	}
	
	public void addLink(Link l){
		neighbors.add(l);
	}
	
	public int getNumber(){
		return number;
	}

	

	public ArrayList<Link> getNeighbors() {
		return neighbors;
	}
	
	public ArrayList<ParseRule> getKernel(){
		return kernel;
	}


	@Override
	public String toString() {
		String result = "State " + number + "\n";
		result += "-------------------------\n";
		for(ParseRule rule : kernel){
			result += rule.toString() + "\n";
		}
		result += "-------------------------\n";
		for(ParseRule rule : nonKernel){
			result += rule.toString() + "\n";
		}
		return result;
	}


	public HashMap<Token, ArrayList<ParseRule>> getTokenRules() {
		return tokenRules;
	}


}
