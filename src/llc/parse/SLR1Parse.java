package llc.parse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import llc.codeGenerator.CodeGenerator;
import llc.codeGenerator.ForUtils;
import llc.codeGenerator.SemanticAction;
import llc.codeGenerator.instruction.Operand;
import llc.exceptions.CompileErrorException;
import llc.exceptions.ImplementationException;
import llc.exceptions.InvalidTokenException;
import llc.lexicalAnalysis.LexicalAnalyzer;
import llc.lexicalAnalysis.tokens.Token;
import llc.lexicalAnalysis.tokens.TokenId;
import llc.lexicalAnalysis.tokens.TokenKeyword;
import llc.lexicalAnalysis.tokens.TokenType;
import llc.symbolTable.SymbolTable;
import parseTable.Action;
import parseTable.GetParseTable;
import parseTable.PTB;
import parseTable.ParseTableGenerator;
import parseTable.SLR1ParseTable;
import cfg.CFG;
import cfg.Rule;

public class SLR1Parse {
	public final static int MAX_INT = 2147483647;
	public final static int MIN_INT = -2147483648;
	private static SLR1ParseTable parseTable = null;
	private LexicalAnalyzer lexical;
	private CodeGenerator codeGen;
	private Stack<Integer> parseStack = new Stack<Integer>();
	private static CFG grammer = null;
	private Token cur, last;
	public Operand endLine = new Operand(0);
	private ForUtils forUtils = new ForUtils();
	private ArrayList<CompileErrorException> excep = new ArrayList<CompileErrorException>();
	
	static{
		try{
			ParseTableGenerator ptg = GetParseTable.getParseTable("input.txt");
			setParseTable( ptg.SLR1ParseTable() );
			setGrammer(ptg.getGrammar());
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}

	public SymbolTable getSymbolTable(){
		return getLexical().getSymbolTable();
	}
	
	public static CFG getGrammer() {
		return grammer;
	}

	public static void setGrammer(CFG grammer) {
		SLR1Parse.grammer = grammer;
	}

	public static SLR1ParseTable getParseTable() {
		return parseTable;
	}

	private static void setParseTable(SLR1ParseTable parseTable) {
		SLR1Parse.parseTable = parseTable;
	}

	public LexicalAnalyzer getLexical() {
		return lexical;
	}

	public void setLexical(LexicalAnalyzer lexical) {
		this.lexical = lexical;
	}

	public CodeGenerator getCodeGen() {
		return codeGen;
	}

	public void setCodeGen(CodeGenerator codeGen) {
		this.codeGen = codeGen;
	}
	
	private PTB makePTBTerminal(Token cur){
		cfg.TokenType tokenType = cfg.TokenType.TERMINAL;
		String name = null;
		if( cur.getType() == TokenType.KEYWORD )
			name = ((TokenKeyword)cur).getKeyword().getLexeme();
		else
			name = cur.getType().toString();
		if( name.equals("EOF") )
			name="$";
//		System.err.println("name="+name);
		cfg.Token cfgToken = new cfg.Token(name, tokenType);
//		System.err.println("parseStack.peek="+parseStack.peek()+ " cfgToken="+cfgToken);
		return parseTable.getPTB(parseStack.peek(), cfgToken);
	}
	
	private PTB makePTBVariable(cfg.Token token){
		return parseTable.getPTB(parseStack.peek(), token);
	}

	public void nextToken() throws InvalidTokenException, IOException, ImplementationException{
		last = cur;
		cur = lexical.nextToken();
	}
	
	public boolean emptyGoto(){
		boolean res = true;
		for( int i=0;i<grammer.getRulesSize();i++ ){
			PTB tmp = makePTBVariable(grammer.getRule(i).getLHS());
			if( tmp == null || tmp.getAction() == null || tmp.getAction() == Action.ERROR )
				continue;
			res = false;
		}
		return res;
	}
	
	private boolean notFollow() {
		for( int i=0;i<grammer.getRulesSize();i++ ){
			PTB tmp = makePTBVariable(grammer.getRule(i).getLHS());
			if( tmp == null || tmp.getAction() == null || tmp.getAction() == Action.ERROR )
				continue;
			int ruleNumber = tmp.getNum();
			ArrayList<cfg.Token> tmpArray = grammer.follow(grammer.getRule(i).getLHS());
			for (cfg.Token token : tmpArray) 
				if( token.getType() == cfg.TokenType.TERMINAL ){
					if( cur.getType() == TokenType.KEYWORD ){
						if( token.getName().equals(((TokenKeyword)cur).getKeyword().getLexeme()) ){
							parseStack.add(ruleNumber);
							return false;
						}else
							continue;
					}
					if( cur.getType() == TokenType.ID ){
						if( token.getName().equals( ((TokenId)cur).getLexeme() ) ){
							parseStack.add(ruleNumber);
							return false;
						}else
							continue;
					}
					if( token.getName().equals(cur.toString()) )
						parseStack.add(ruleNumber);
						return false;
				}
		}
		return true;
	}
	
	public void parse() throws CompileErrorException, IOException, ImplementationException, InvalidTokenException{
		
		parseStack.push(0);
		cur = null;//FIXME for last
	
		try {
			
			nextToken();
			while(true){
				
				PTB ptb = makePTBTerminal(cur);
				
				if( ptb.getAction().equals(Action.SHIFT) ){
					parseStack.push(ptb.getNum());
					System.out.println("Shifted "+cur);
					if( cur.getType()==TokenType.INT_NUM || cur.getType()==TokenType.FLOAT_NUM ){
						if( cur.getType()==TokenType.INT_NUM ){//check it
							String integer = (String) cur.getAttr();
							InvalidTokenException invalid = new InvalidTokenException(cur.getPosition(), "there is no space to store it into int type");
							if( integer.length()>11 )
								throw invalid;
							Integer tmp = new Integer(integer);
							if( tmp > MAX_INT || tmp < MIN_INT )
								throw invalid;
						}else{
							//FIXME any other idea?
							String integer = (String) cur.getAttr();
							InvalidTokenException invalid = new InvalidTokenException(cur.getPosition(), "there is no space to store it into float type");
							if( integer.length()>11 )
								throw invalid;
							Float tmp = new Float(integer);
							if( tmp > MAX_INT || tmp < MIN_INT )
								throw invalid;
						}
					}
					nextToken();
					
				}else if( ptb.getAction().equals(Action.REDUCE) ){
					int num = ptb.getNum();
					Rule rule = grammer.getRule(num);
					System.out.println("Reduced rule="+rule);
					int numOfPops = rule.getRHS().size();
					for( int i=0;i<numOfPops;i++ )
						parseStack.pop();
					PTB ptbTmp = makePTBVariable(rule.getLHS());
					if(ptbTmp.getAction() == Action.ERROR)
						throw new ImplementationException("Empty Cell in GOTO");
					parseStack.push(ptbTmp.getNum());
					
					SemanticAction semanticAction = codeGen.getSemanticActionManager().getAction(rule.getNum());
					
					if (semanticAction != null)
						semanticAction.run(this);
					
				}else if( ptb.getAction().equals(Action.ACCEPT) ){
					System.out.println("Accepted");
					break;
				}else{
					throw new InvalidTokenException(cur.getPosition(), cur.toString());
				}
			}
		} catch (InvalidTokenException e) {
//			//FIXME panic mode
			while( parseStack.isEmpty() == false && emptyGoto() == true )
				parseStack.pop();
			while( cur.getType() !=  TokenType.EOF && notFollow() == true )
					nextToken();
			System.err.println("cur="+cur);
			excep.add( new CompileErrorException(e.getPosition(), "Invalid Token "+ e.getMessage()) );
			this.parse();
			return ;
		} catch (ImplementationException e) {
			e.printStackTrace();
		}

		if( excep.isEmpty() == false ){
			for( int i=0;i<excep.size();i++ ){
				CompileErrorException e = excep.get(i);
				System.out.println(e.getPosition() + " Invalid Token " + e.getMessage() );
			}
			throw excep.get(0);
		}
		
		if ( excep.isEmpty() && codeGen.getSemanticStack().size()>0)
			throw new ImplementationException("Stack should be empty");

	}

	public Token getCurrentToken() {
		return cur;
	}

	public Token getLastToken() {
		return last;
	}

	public ForUtils getForUtils() {
		return forUtils;
	}

	public void setForUtils(ForUtils forUtils) {
		this.forUtils = forUtils;
	}
}
