package llc.parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import llc.codeGenerator.SemanticAction;

public class SemanticActionManager {
	HashMap<Integer, SemanticAction> actions = new HashMap<Integer, SemanticAction>();

	public SemanticActionManager() {
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File("actions.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int n = scanner.nextInt();
		for (int i=0; i<n; i++){
			int ruleNumber = scanner.nextInt();
			String action = scanner.next();
			actions.put(ruleNumber, SemanticAction.valueOf(action));
		}
	}
	
	public SemanticAction getAction(int ruleNumber){
		return actions.get(ruleNumber);
	}

}
