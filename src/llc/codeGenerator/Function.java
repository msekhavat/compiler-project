package llc.codeGenerator;

import java.util.ArrayList;

import llc.codeGenerator.instruction.Mod;
import llc.codeGenerator.instruction.Operand;
import llc.codeGenerator.instruction.Type;
import llc.symbolTable.Symbol;

//TODO: design needs refactor: should not extend Symbol 
public class Function extends SymbolReference{
	//ruturn value is in 0 of global
	public static final Operand funcReturnGlobal = new Operand(Mod.GD, Type.INT, 4);
	public static final Operand funcSPGlobal = new Operand(Mod.GD, Type.INT, 8);
	public static final Operand funcReturnLocArg = new Operand(Mod.LD, Type.INT, 0);
	public static final Operand funcStackPrev = new Operand(Mod.LD, Type.INT, 4);
	public static final Operand funcStackNew = new Operand(Mod.LD, Type.INT, 8);
	public Function(Symbol symbol) {
		super(symbol);
	}

	ArrayList<Variable> args = new ArrayList<Variable>();
	ArrayList<Operand> initLocs = new ArrayList<Operand>();
	private ActivationRecord activationRecord;
	
	public void addArgument(Variable v, Operand argInitLoc) {
		args.add(v);
		initLocs.add(argInitLoc);
	}

	public ArrayList<Variable> getArgs() {
		return args;
	}

	public Operand getArgLoc(int index){
		return initLocs.get(index);
	}

	public Operand getReturnValueLocation(){
		return new Operand(Mod.GD, getType(), 0);
	}

	public void setActivationRecord(ActivationRecord activationRecord) {
		this.activationRecord = activationRecord;
	}
	
	public ActivationRecord getActivationRecord(){
		return activationRecord;
	}
	
	public boolean isMain(){
		return isGlobal() && symbol.getLexeme().equals("main");
	}
}

