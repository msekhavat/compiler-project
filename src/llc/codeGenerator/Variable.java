package llc.codeGenerator;

import java.util.ArrayList;

import llc.codeGenerator.instruction.Mod;
import llc.codeGenerator.instruction.Operand;
import llc.codeGenerator.instruction.Type;
import llc.symbolTable.Symbol;

public class Variable extends SymbolReference{
	private ArrayList<Integer> dimension = new ArrayList<Integer>();
	private int address;

	public Variable(Symbol symbol) {
		super(symbol);
	}

	public ArrayList<Integer> getDimension() {
		return dimension;
	}
	
	public void addDimension(int size){
		dimension.add(size);
	}
	
	public int getSize(){
		int ret = symbol.getType().getSize();
		for (Integer d: dimension)
			ret *= d;
		return ret;
	}

	public int getAddress() {
		return address;
	}

	public void setAddress(int address) {
		this.address = address;
	}

	public Type getType() {
		return getSymbol().getType();
	}

	public Operand getOperand(){
		return new Operand((isGlobal() ? Mod.GD : Mod.LD), symbol.getType(), address);
	}
	
	@Override
	public String toString() {
		return "var "+symbol;
	}
}
