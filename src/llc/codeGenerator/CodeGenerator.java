package llc.codeGenerator;

import java.io.PrintStream;

import llc.parse.SemanticActionManager;

public class CodeGenerator {
	private SemanticStack semanticStack = new SemanticStack();

	private PrintStream out;
	
	private GeneratedInstruction instructionBuffer = new GeneratedInstruction();

	private SemanticActionManager semanticActionManager = new SemanticActionManager();

	private ActivationRecordManager activationRecordManager = new ActivationRecordManager();
	
	public CodeGenerator(PrintStream writer ) {
		this.out=writer;
	}

	public PrintStream getOut() {
		return out;
	}

	public SemanticStack getSemanticStack() {
		return semanticStack;
	}

	public SemanticActionManager getSemanticActionManager() {
		return semanticActionManager;
	}

	public void writeInstructionInOutputFile(){
		out.println(getActivationRecordManager().getGlobal().getSize());
		out.println(getActivationRecordManager().getMain().getSize());
		out.println(getActivationRecordManager().getMain().getBegin());
		System.err.println("top="+instructionBuffer.getTop());
		for( int i=0;i<instructionBuffer.getTop();i++ )
			out.println(instructionBuffer.getIndex(i));
	}

	public GeneratedInstruction getInstructionBuffer() {
		return instructionBuffer;
	}

	public void setInstructionBuffer(GeneratedInstruction instructionBuffer) {
		this.instructionBuffer = instructionBuffer;
	}

	public ActivationRecordManager getActivationRecordManager() {
		return activationRecordManager;
	}

}
