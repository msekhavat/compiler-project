package llc.codeGenerator;

public class ActivationRecordManager {
	ActivationRecord global = new ActivationRecord(null, 0);
	ActivationRecord current = global;
	private ActivationRecord main = null;
	
	public ActivationRecord openNewRecord(Function function, int begin){
		current = new ActivationRecord(function, begin);
		if (function.isMain()){
			setMain(current);
		}
		return current;
	}
	
	public void closeCurrentRecord(){
		current = global;
	}

	public ActivationRecord getCurrent() {
		return current;
	}
	
	public ActivationRecord getGlobal(){
		return global;
	}

	public ActivationRecord getMain() {
		return main;
	}

	public void setMain(ActivationRecord main) {
		this.main = main;
	}

}
