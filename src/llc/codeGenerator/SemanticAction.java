package llc.codeGenerator;

import java.util.ArrayList;

import llc.codeGenerator.instruction.Instruction;
import llc.codeGenerator.instruction.Mod;
import llc.codeGenerator.instruction.Opcode;
import llc.codeGenerator.instruction.Operand;
import llc.codeGenerator.instruction.Type;
import llc.exceptions.CompileErrorException;
import llc.exceptions.ImplementationException;
import llc.lexicalAnalysis.tokens.Keyword;
import llc.lexicalAnalysis.tokens.Token;
import llc.lexicalAnalysis.tokens.TokenId;
import llc.lexicalAnalysis.tokens.TokenType;
import llc.parse.SLR1Parse;
import llc.symbolTable.Symbol;
import llc.symbolTable.SymbolTable;


public enum SemanticAction {
	pointerInit, pointerPush, pointerDim,
	/**
	 * create (open) scope for it
	 */
	startBlock,
	/**
	 * close scope
	 */
	endBlock, 

	/**
	 * Tell currnt scope of SymbolTable that a new symbol will be defined for it
	 */
	defSym, 

	/**
	 * push Symbol for current tokenId into ss
	 */
	pushId,
	/**
	 * push Integer Type into ss
	 */
	integerType, 
	/**
	 * push boolean Type into ss
	 */
	boolType, 
	/**
	 * push float Type into ss
	 */
	floatType,
	/**
	 * push void Type! into ss
	 */
	voidType, 
	/**
	 * push size of row of array into ss
	 */
	insertSize, 
	/**
	 * changed it to function
	 */
	function,
	/**
	 * changed it to variable
	 */
	variable,
	/**
	 * allocate memory for variable
	 */
	mallocVariable, 
	/**
	 * handling if statement start
	 */
	startIf,
	/**
	 * handling if statement end of block
	 */
	endBlockIf,
	/**
	 * handling if statement end of rest
	 */
	endRestIf,
	/**
	 * handling ||
	 */
	handleCondOpOr,
	/**
	 * handling &&
	 */
	handleCondOpAnd,
	/**
	 * handling ==
	 */
	handleEqE,
	/**
	 * handling !=
	 */
	handleEqN,
	/**
	 * handling <
	 */
	handleRelLes,
	/**
	 * handling >
	 */ 
	handleRelGre,
	/**
	 * handling <=
	 */
	handleRelLeE,
	/**
	 * handling >=
	 */
	handleRelGrE, 
	/**
	 * handling +
	 */
	handleArithOpPl,
	/**
	 * handling -
	 */
	handleArithOpMi,
	/**
	 * handling *
	 */
	handleArithOpMu,
	/**
	 * handling /
	 */
	handleArithOpDi,
	/**
	 * handling %
	 */
	handleArithOpMo,
	/**
	 * handling unary !
	 */
	handleLogicalNot,
	/**
	 * handling unary -
	 */
	handleUnaryMinus,
	/**
	 * handling basicTerm int_num, float_num, true, false
	 */
	handleBasicTerm,
	/* add arrgument to semantic stack
	 * 
	 * also put space for instruction on init value
	 */
	defArg, 
	/**
	 * open new ActivationRecord
	 */
	openRecord, 
	/**
	 * close current ActivationRecord
	 */
	closeRecord, 
	/**
	 * pops from top of ss
	 */
	pop,
	/**
	 * handling assignment
	 */
	handleAssign,
	/**
	 * handling read float
	 */
	handleReadFloat,
	/**
	 * handling read integer
	 */
	handleReadInt,
	/**
	 * handling write float
	 */
	handleWriteFloat,
	/**
	 * handling write integer
	 */
	handleWriteInt,
	/**
	 * handling start of for
	 */
	startFor,
	/**
	 * handling end of for
	 */
	endFor,
	/**
	 * push top to stack to jump 
	 */
	pushAddrFor,
	/**
	 * push free space to stack to jump 
	 */
	pushFreeSpace,
	/**
	 * push end of ) for
	 */
	endAssignFor,
	/**
	 * start of for should check expr
	 */
	startOfBlockFor,
	/**
	 * set the Continue Addr
	 */
	pushContinueAddrFor,
	/**
	 * handling break;
	 */
	breakFor,
	/**
	 * handling continue;
	 */
	continueFor,
	pushCall, call, retExp, retVoid, lastLine,
	;

	public void run(SLR1Parse parser) throws CompileErrorException, ImplementationException{
		CodeGenerator codeGen = parser.getCodeGen();
		parser.getLexical();
		SymbolTable symbolTable = parser.getSymbolTable();
		ForUtils forUtils = parser.getForUtils();
		Token current = parser.getCurrentToken();
		Token last = parser.getLastToken();
		SemanticStack ss = codeGen.getSemanticStack();
		GeneratedInstruction bufferIns = codeGen.getInstructionBuffer();
		ActivationRecord record = codeGen.getActivationRecordManager().getCurrent();
		ActivationRecord globalRecord = codeGen.getActivationRecordManager().getGlobal();
		Object top = ss.getTop();
		System.err.println("current.getType()="+current.getType()+" current.getAttr()="+current.getAttr());
		
		switch(this){
		case pointerInit:
			Operand arrayOffset = null;
			ss.push(current.getPosition().getLineNumber()); //line of TokenLocation
			ss.push(arrayOffset);
			ss.push(0);
			break;
		case pointerDim:
			Operand reqOffset = (Operand) ss.getTop();
			if (reqOffset.getType() != Type.INT)
				throw new CompileErrorException(current.getPosition(), "Array offset should be of type integer");
			ss.pop();
			
			int dimNum = (Integer) ss.getTop(1);
			arrayOffset = (Operand) ss.getTop(2);
			if (dimNum == 0){
				arrayOffset = getTemp(record);
				bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, new Operand(0), arrayOffset));
			}
			ss.pop(2);
			
			int reqOffsetCodeLine = (Integer) ss.getTop(1);
			Variable baseVar = getTopVariable(ss, 2);

			ArrayList<Integer> varDims = baseVar.getDimension();
			
			if (dimNum >= varDims.size())
				throw new CompileErrorException(last.getPosition(), "dimensions more than defined!");
			
			Operand reqOffsetIsGE = getTemp(record, Type.BOOL);
			Operand sizeOfDim = new Operand(varDims.get(dimNum));
			bufferIns.addInstruction(new Instruction(Opcode.GE, reqOffset, sizeOfDim, reqOffsetIsGE));
			
			Operand cDimProgramCounter = getTemp(record);
			bufferIns.addInstruction(new Instruction(Opcode.PCV, cDimProgramCounter));
			bufferIns.addInstruction(new Instruction(Opcode.ADD, cDimProgramCounter, new Operand(4), cDimProgramCounter));
			
			bufferIns.addInstruction(new Instruction(Opcode.JZ, reqOffsetIsGE, cDimProgramCounter));
			
			bufferIns.addInstruction(new Instruction(Opcode.ERR, new Operand(reqOffsetCodeLine), new Operand(0)));
			
			if (dimNum>0){
				Operand coeff = new Operand(varDims.get(dimNum));
				bufferIns.addInstruction(new Instruction(Opcode.MULT, coeff, arrayOffset, arrayOffset));
			}
			bufferIns.addInstruction(new Instruction(Opcode.ADD, reqOffset, arrayOffset, arrayOffset));
			
			ss.push(arrayOffset);
			ss.push(dimNum+1);
			break;
		case pointerPush:
			dimNum = (Integer) ss.getTop(1);
			arrayOffset = (Operand) ss.getTop(2);
			ss.pop(2);
			
			reqOffsetCodeLine = (Integer) ss.getTop(1);
			baseVar = getTopVariable(ss, 2);
			ss.pop(2);
			
			if (dimNum != baseVar.getDimension().size())
				throw new CompileErrorException(current.getPosition(), "dimensions less than defined!");
			if (dimNum>0){
				bufferIns.addInstruction(new Instruction(Opcode.MULT, arrayOffset, new Operand(baseVar.getType().getSize()), arrayOffset));
				bufferIns.addInstruction(new Instruction(Opcode.ADD, arrayOffset, new Operand(baseVar.getAddress()), arrayOffset));
				ss.push(new Operand(baseVar.isGlobal()? Mod.GI : Mod.LI, baseVar.getType(), arrayOffset.getValue()));
			} else {
				ss.push(baseVar.getOperand());
			}
			break;
		case startBlock:
			symbolTable.openNewScope();
			break;
		case endBlock:
			symbolTable.closeLastScope();
			break;
		case defSym://before it we insert a type into ss 
			symbolTable.setDefiningNewSymbol((Type) top);
			ss.pop();
			break;
		case pushId:
			Symbol symbol = symbolTable.getSymbol(((TokenId)current).getLexeme(), current.getPosition());
			ss.push(symbol);
			break;
		case integerType:
			ss.push(Type.INT);
			break;
		case boolType:
			ss.push(Type.BOOL);
			break;
		case floatType:
			ss.push(Type.FLOAT);
			break;
		case voidType:
			ss.push(Type.VOID);
			break;
		case function:
			Function function = new Function((Symbol) top);
			ss.pop();
			ss.push(function);
			break;
		case variable:
			Variable variable = new Variable((Symbol) top);
			ss.pop();
			ss.push(variable);
			break;
		case insertSize:
			Variable variableArray = (Variable) top;
			variableArray.addDimension(new Integer((String)current.getAttr()));//current == int_num
			break;
		case defArg:
			Function func = record.getFunction();
			Variable varArg = (Variable) top;
			ss.pop();
			
			if (func.isMain())
				throw new CompileErrorException(last.getPosition(), "main must not have arguments");
			varArg.setAddress(record.malloc(varArg.getType()));
			
			Operand argInitLoc = getTemp(globalRecord, varArg.getType());
			func.addArgument(varArg, argInitLoc);
			
			bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, argInitLoc, varArg.getOperand()));
			break;
		case openRecord:
			func = (Function) top;
			ss.pop();

			if (func.isMain() && func.getType() != Type.VOID)
				throw new CompileErrorException(func.getSymbol().getPositionOfDefinition(), "main should be void");
			record = codeGen.getActivationRecordManager().openNewRecord(func, bufferIns.getTop());
			
			Operand unknownActivationSize = new Operand(0);
			bufferIns.addInstruction(new Instruction(Opcode.SPV, Function.funcSPGlobal));
			bufferIns.addInstruction(new Instruction(Opcode.ADD, unknownActivationSize, Function.funcSPGlobal, Function.funcSPGlobal));
			bufferIns.addInstruction(new Instruction(Opcode.SPASSIGN, Function.funcSPGlobal));
			bufferIns.addInstruction(new Instruction(Opcode.SUB, Function.funcSPGlobal, unknownActivationSize, Function.funcStackPrev));
			bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, Function.funcReturnGlobal, Function.funcReturnLocArg));
			
			ss.push(unknownActivationSize);
			break;
		case closeRecord:
			if (record.getFunction().getType() == Type.VOID){
				retVoid.run(parser);
			} else {
				bufferIns.addInstruction(new Instruction(Opcode.ERR, new Operand(last.getPosition().getLineNumber()), new Operand(1)));
			}
			unknownActivationSize = (Operand) top;
			ss.pop();
			
			unknownActivationSize.setValue(record.getSize());
			codeGen.getActivationRecordManager().closeCurrentRecord();
			System.err.println("ss.size()="+ss.size());
			break;
		case pushCall:
			Symbol symTop = (Symbol) top;
			if (!(symTop.getAttr() instanceof Function))
				throw new CompileErrorException(current.getPosition(), "non-function token is used for function call");
			ss.push(Flag.BEGIN_ARGS);
			break;
		case call:
			ArrayList<Object> args = Flag.BEGIN_ARGS.collectStack(ss);
			symTop = (Symbol) ss.getTop();
			func = (Function) symTop.getAttr();

			ss.pop();
			
			if (args.size() != func.getArgs().size())
				throw new CompileErrorException(current.getPosition(), "number of arguments is wrong");
			for (int i=0; i<args.size(); i++){
				Operand arg = (Operand) args.get(i);
				if (arg.getType() != func.getArgs().get(i).getType())
					throw new CompileErrorException(current.getPosition(), i+" th argument type mismatch");
				bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, arg, func.getArgLoc(i)));
			}
			
			Operand returnPlace = getTemp(record);
			bufferIns.addInstruction(new Instruction(Opcode.PCV, returnPlace));
			bufferIns.addInstruction(new Instruction(Opcode.ADD, returnPlace, new Operand(3), Function.funcReturnGlobal));
			bufferIns.addInstruction(new Instruction(Opcode.JMP, new Operand(func.getActivationRecord().getBegin())));
			
			if (func.getType() != Type.VOID){
				Operand returnedValue = getTemp(record, func.getType());
				bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, func.getReturnValueLocation(), returnedValue));
				ss.push(returnedValue);
			} else {
				ss.push(new Operand(Mod.GD, Type.VOID, 1));
			}
			break;
		case retExp:
			Operand exp = (Operand) ss.pop();
			func = record.getFunction();
			if (exp.getType() != func.getType() || func.getType()==Type.VOID)
				throw new CompileErrorException(current.getPosition(), "return type mismatch");
			
			bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, exp, func.getReturnValueLocation()));
			bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, Function.funcReturnLocArg, Function.funcReturnGlobal));
			bufferIns.addInstruction(new Instruction(Opcode.SPASSIGN, Function.funcStackPrev));
			bufferIns.addInstruction(new Instruction(Opcode.JMP, Function.funcReturnGlobal));
			
			break;
		case retVoid:
			func = record.getFunction();
			if (func.getType() != Type.VOID)
				throw new CompileErrorException(current.getPosition(), "return type mismatch");
			bufferIns.addInstruction(new Instruction(Opcode.ASSIGN, Function.funcReturnLocArg, Function.funcReturnGlobal));
			bufferIns.addInstruction(new Instruction(Opcode.SPASSIGN, Function.funcStackPrev));
			bufferIns.addInstruction(new Instruction(Opcode.JMP, func.isMain() ? parser.endLine  : Function.funcReturnGlobal));
			break;
		case mallocVariable:
			Variable var = (Variable) top;
			int size = var.getSize();
			if( size > SLR1Parse.MAX_INT )
				throw new CompileErrorException(current.getPosition(), "too many memory allocation needed");
			
			int address = record.malloc(var);
			var.setAddress(address);
			ss.pop(); // pops variable
			ss.push(var.getType()); //push back type. ex: int a,b[10],c;
			break;
		case lastLine:
			parser.endLine.setValue(bufferIns.getTop());
			break;
		case startIf:
			System.err.println("startIF bufferIns.size()="+bufferIns.getTop());
			//check the expr bool
			Operand ifExpr = (Operand) top;
			if( ifExpr.getType() != Type.BOOL ){
				ss.pop();
				throw new CompileErrorException(last.getPosition(), "the expr before this block should be boolean");
			}
			pushFreeSpaceTop(ss, bufferIns);
			break;
		case endBlockIf:
			int indexOfJmpZero = (Integer)ss.getTop();
			ss.pop();
			System.err.println("endBlockIf indexOfJmpZero="+indexOfJmpZero);
			ifExpr = ss.getOperandAndPop();
			pushFreeSpaceTop(ss, bufferIns);
			Instruction ifJmpZero = new Instruction(Opcode.JZ);
			ifJmpZero.addOperand(ifExpr);
			ifJmpZero.addOperand(new Operand(Mod.IM, Type.INT, bufferIns.getTop()));
			bufferIns.addInstruction(ifJmpZero, indexOfJmpZero);
			break;
		case endRestIf:
			int indexOfJmp = (Integer)ss.getTop();
			ss.pop();
			Instruction ifJmp = new Instruction(Opcode.JMP);
			ifJmp.addOperand(new Operand(Mod.IM, Type.INT, bufferIns.getTop()));
			bufferIns.addInstruction(ifJmp, indexOfJmp);
			break;
		case pop:
			ss.pop();
			break;
		case handleCondOpOr:
			handleCond(last, ss, bufferIns, record, Opcode.OR);
			break;
		case handleCondOpAnd:
			handleCond(last, ss, bufferIns, record, Opcode.AND);
			break;
		case handleEqE:
			handleEquals(last, ss, bufferIns, record, Opcode.EQ);
			break;
		case handleEqN:
			handleEquals(last, ss, bufferIns, record, Opcode.NEQ);
			break;
		case handleRelLes:
			handleArithRel(last, ss, bufferIns, record, Opcode.LT, Type.BOOL);
			break;
		case handleRelGre:
			handleArithRel(last, ss, bufferIns, record, Opcode.GT, Type.BOOL);
			break;
		case handleRelLeE:
			handleArithRel(last, ss, bufferIns, record, Opcode.LE, Type.BOOL);
			break;
		case handleRelGrE:
			handleArithRel(last, ss, bufferIns, record, Opcode.GE, Type.BOOL);
			break;
		case handleArithOpPl:
			handleArithRel(last, ss, bufferIns, record, Opcode.ADD, null);
			break;
		case handleArithOpMi:
			handleArithRel(last, ss, bufferIns, record, Opcode.SUB, null);
			break;
		case handleArithOpMu:
			handleArithRel(last, ss, bufferIns, record, Opcode.MULT, null);
			break;
		case handleArithOpDi:
			handleArithRel(last, ss, bufferIns, record, Opcode.DIV, null);
			break;
		case handleArithOpMo:
			handleArithRel(last, ss, bufferIns, record, Opcode.MOD, null);
			break;
		case handleLogicalNot:
			handleUnary(last, ss, bufferIns, record, Opcode.NOT);
			break;
		case handleUnaryMinus:
			handleUnary(last, ss, bufferIns, record, Opcode.MIN);
			break;
		case handleBasicTerm:
			Operand basicTerm = new Operand(Mod.IM, null, "null");
			if( current.getType() == TokenType.INT_NUM ){
				basicTerm.setValue(current.getAttr());
				basicTerm.setType(Type.INT);
			}
			else if( current.getType() == TokenType.FLOAT_NUM ){
				basicTerm.setValue(current.getAttr());
				basicTerm.setType(Type.FLOAT);
			}
			else if( current.getType() == TokenType.KEYWORD && ( (Keyword)current.getAttr() == Keyword.TRUE || (Keyword)current.getAttr() == Keyword.FALSE ) ){
				basicTerm.setValue( ((Keyword)current.getAttr()).getLexeme() );
//				System.err.println( "begir=" + ((Keyword)current.getAttr()).getLexeme() );
				basicTerm.setType(Type.BOOL);
			}
			else
				throw new ImplementationException("can't should be other type in basicTerm only int, float, bool are valid");
			ss.push(basicTerm);
			break;
		case handleAssign:
			handleAssignment(last, ss, bufferIns, record);
			break;
		case handleReadFloat:
			handleInputOutput(last, ss, bufferIns, record, Opcode.RFLOAT);
			break;
		case handleReadInt:
			handleInputOutput(last, ss, bufferIns, record, Opcode.RINT);
			break;
		case handleWriteFloat:
			handleInputOutput(last, ss, bufferIns, record, Opcode.WFLOAT);
			break;
		case handleWriteInt:
			handleInputOutput(last, ss, bufferIns, record, Opcode.WINT);
			break;
		case startFor:
			forUtils.startFor();
			break;
		case pushAddrFor:
			ss.push(new Integer(bufferIns.getTop()));
			break;
		case pushFreeSpace:
			pushFreeSpaceTop(ss, bufferIns);
			break;
		case pushContinueAddrFor:
			System.err.println("pushContinueAddrFor continueAddr="+bufferIns.getTop());
			forUtils.getForProperty().lastElement().setContinueAddr(new Integer(bufferIns.getTop()));
			break;
		case endAssignFor:
			Object first = ss.getTop();
			ss.pop();
			Object Second = ss.getTop();
			ss.pop();
			int addrJmpFor = (Integer)ss.getTop();
			ss.pop();
			bufferIns.addInstruction(new Instruction(Opcode.JMP, new Operand(Mod.IM, Type.INT, addrJmpFor)));
			ss.push(Second);
			ss.push(first);
			break;
		case breakFor:
			handlingForState(ForStatement.Break, last, bufferIns, forUtils);
			break;
		case continueFor:
			handlingForState(ForStatement.Continue, last, bufferIns, forUtils);
			break;
		case startOfBlockFor:
			int addrInsJmpFor = (Integer)ss.getTop();
			ss.pop();
			Operand exprFor = (Operand) ss.getTop();
			if( exprFor.getType() != Type.BOOL ){
				throw new CompileErrorException(last.getPosition(), "the expression of for should be boolean type");
			}
			bufferIns.addInstruction( new Instruction(Opcode.JMP, new Operand(Mod.IM, Type.INT, bufferIns.getTop())), addrInsJmpFor);
			pushFreeSpaceTop(ss, bufferIns);
			break;
		case endFor:
			bufferIns.addInstruction(new Instruction(Opcode.JMP, new Operand(forUtils.getForProperty().lastElement().getContinueAddr())));//ZZZ
			System.err.println("endFor breakAddr="+bufferIns.getTop());
			forUtils.getForProperty().lastElement().setBreakAddr(bufferIns.getTop());
			int firstBlockForAddr = (Integer) ss.getTop();
			ss.pop();
			bufferIns.addInstruction( new Instruction(Opcode.JZ, ss.getOperandAndPop(), new Operand(forUtils.getForProperty().lastElement().getBreakAddr())), 
					firstBlockForAddr);
			// نوشتن continue و break
			while( forUtils.getForState().lastElement().isEmpty() == false ){
				ForStatement forState = forUtils.getForState().lastElement().pop();
				int addr = forUtils.getAddressOfForState().lastElement().pop();
				switch (forState) {
				case Break:
					bufferIns.addInstruction(new Instruction(Opcode.JMP, new Operand(forUtils.getForProperty().lastElement().getBreakAddr())), addr);
					break;
				case Continue:
					bufferIns.addInstruction(new Instruction(Opcode.JMP, new Operand(forUtils.getForProperty().lastElement().getContinueAddr())), addr);
					break;
				}
			}
			forUtils.endFor();
			break;
		}
		System.err.println("ss.size()="+ss.size());
	}

	private void handlingForState(ForStatement forState, Token last,
			GeneratedInstruction bufferIns, ForUtils forUtils) throws CompileErrorException {
		if( forUtils.isEmpty() )
			throw new CompileErrorException(last.getPosition(), "there is no for here!");
		forUtils.getForState().lastElement().add(forState);
		forUtils.getAddressOfForState().lastElement().add(bufferIns.getTop());
		bufferIns.increaseTop();
	}

	private Variable getTopVariable(SemanticStack ss, int index) throws CompileErrorException {
		Symbol symbol = (Symbol) ss.getTop(index);
		Object top = symbol.getAttr();
		if (!(top instanceof Variable))
			throw new CompileErrorException(symbol.getPositionOfDefinition(), "used function in place of variable");
		return (Variable)top;
	}	

	private void handleInputOutput(Token last, SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record,
			Opcode op) throws CompileErrorException {
		Operand expr = ss.getOperandAndPop();
		boolean isFloat = false;
		if( op == Opcode.WFLOAT || op == Opcode.RFLOAT )
			isFloat = true;
		if( isFloat && expr.getType() != Type.FLOAT ){
			throw new CompileErrorException(last.getPosition(), "you want float operation, not other type like Int, before this");
		}
		if( !isFloat && expr.getType() != Type.INT ){
			throw new CompileErrorException(last.getPosition(), "you want int operation, not other type like Float, before this");
		}
		
		Instruction inOut = new Instruction(op);
		inOut.addOperand(expr);
		bufferIns.addInstruction(inOut);
	}

	private void handleAssignment(Token last, SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record) throws CompileErrorException {
//		for( int i=0;i<ss.size();i++ )
//			System.err.println("ss[i]="+ss.get(i));
		Operand expr1 = ss.getOperandAndPop();
		Operand expr2 = ss.getOperandAndPop();
		System.err.println("bufferIns.size()="+bufferIns.getTop()+" expr1="+expr1+" expr2="+expr2);
		if( expr1.getType() != expr2.getType() || expr1.getType() == Type.VOID ){
			System.err.println("expr1.getType()="+expr1.getType()+" expr2.getType()="+expr2.getType());
			throw new CompileErrorException(last.getPosition(), "should have same type or should have value before this");
		}
		
		Instruction assignIns = new Instruction(Opcode.ASSIGN);
		assignIns.addOperand(expr1);
		assignIns.addOperand(expr2);
		bufferIns.addInstruction(assignIns);
		System.err.println("assignIns="+assignIns);
		
	}

	private void handleUnary(Token last, SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record, Opcode op) throws CompileErrorException {
		Operand expr = ss.getOperandAndPop();
		if( op == Opcode.NOT && expr.getType() != Type.BOOL ){
			throw new CompileErrorException(last.getPosition(), "should have Boolean Type before this");
		}
		if( op == Opcode.MIN && expr.getType() != Type.INT && expr.getType() != Type.FLOAT ){
			throw new CompileErrorException(last.getPosition(), "should have Int or Float Type in unary -, before this");
		}

		Type resType = Type.BOOL;
		if( op == Opcode.MIN )
			resType = expr.getType();

		Operand res = new Operand(Mod.LD, resType, record.malloc(resType));
		Instruction orIns = new Instruction(op);
		orIns.addOperand(expr);
		orIns.addOperand(res);
		bufferIns.addInstruction(orIns);
		ss.push(res);
	}

	private void handleArithRel(Token last, SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record, Opcode op,
			Type res) throws CompileErrorException {//res == null => arithmetic
		Operand expr1 = ss.getOperandAndPop();
		Operand expr2 = ss.getOperandAndPop();
		if( ( expr1.getType() != Type.INT && expr1.getType() != Type.FLOAT ) || ( expr1.getType() != Type.INT && expr1.getType() != Type.FLOAT ) ){
			System.err.println("expr1.getType()="+expr1.getType()+" expr2.getType()="+expr2.getType());
			throw new CompileErrorException(last.getPosition(), "should have Int or Float Type before this");
		}
		if( res == null )//arithmetic
			res = (expr1.getType() == Type.FLOAT || expr2.getType() == Type.FLOAT) ? Type.FLOAT : Type.INT ;
		addExprInstructionAndPushResult(ss, bufferIns, record, op, expr2, expr1, res);
	}

	private void handleEquals(Token last, SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record, Opcode eqOrneq) throws CompileErrorException {
		Operand expr1 = ss.getOperandAndPop();
		Operand expr2 = ss.getOperandAndPop();
		if( expr1.getType() != expr2.getType() || expr1.getType() == Type.VOID ){
			throw new CompileErrorException(last.getPosition(), "should have same type or should have value before this");
		}
		addExprInstructionAndPushResult(ss, bufferIns, record, eqOrneq, expr1, expr2, Type.BOOL);
	}

	private void handleCond(Token last, SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record, Opcode andOr)
					throws CompileErrorException {
		Operand expr1;
		Operand expr2;
		expr1 = ss.getOperandAndPop();
		expr2 = ss.getOperandAndPop();
		if( expr1.getType() != Type.BOOL || expr2.getType() != Type.BOOL ){
			throw new CompileErrorException(last.getPosition(), "the expr before this should be boolean");
		}
		addExprInstructionAndPushResult(ss, bufferIns, record, andOr, expr1, expr2, Type.BOOL);
	}


	private void addExprInstructionAndPushResult(SemanticStack ss,
			GeneratedInstruction bufferIns, ActivationRecord record, Opcode op,
			Operand expr1, Operand expr2, Type typeRes) {
		Operand res = new Operand(Mod.LD, typeRes, record.malloc(typeRes));
		Instruction orIns = new Instruction(op);
		orIns.addOperand(expr1);
		orIns.addOperand(expr2);
		orIns.addOperand(res);
		bufferIns.addInstruction(orIns);
		ss.push(res);
	}

	private void pushFreeSpaceTop(SemanticStack ss, GeneratedInstruction bufferIns) {
		System.err.println("pushTop top="+bufferIns.getTop());
		ss.push(new Integer(bufferIns.getTop()));
		bufferIns.increaseTop();//push i
		System.err.println("pushTop End top="+bufferIns.getTop());
	}
	
	private Operand getTemp(ActivationRecord record, Type type){
		int address = record.malloc(type);
		return new Operand(record.isGlobal() ? Mod.GD : Mod.LD, type, address);
	}
	
	private Operand getTemp(ActivationRecord record){
		return getTemp(record, Type.INT);
	}
	
}
