package llc.codeGenerator;

import java.util.Stack;

public class ForUtils {
	private Stack< Stack<ForStatement> > forState = null;
	private Stack< Stack<Integer> > addressOfForState = null;
	private Stack< Pair > forProperty = null;
	
	public ForUtils(){
		forState = new Stack< Stack<ForStatement> >();
		addressOfForState = new Stack< Stack<Integer> >();
		forProperty = new Stack< Pair >();
	}
	
	public void endFor(){
		getForState().pop();
		getAddressOfForState().pop();
		getForProperty().pop();
	}
	
	public void startFor(){
		getForState().add( new Stack<ForStatement>() );
		getAddressOfForState().add( new Stack<Integer>() );
		getForProperty().add(new Pair());
	}
	
	public boolean isEmpty(){
		return getForState().isEmpty();
	}

	public Stack< Stack<ForStatement> > getForState() {
		return forState;
	}

	public Stack< Stack<Integer> > getAddressOfForState() {
		return addressOfForState;
	}

	public Stack< Pair > getForProperty() {
		return forProperty;
	}

}


