package llc.codeGenerator;

import java.util.ArrayList;
import java.util.Collections;

public enum Flag {
	BEGIN_ARGS;
	
	public ArrayList<Object> collectStack(SemanticStack ss){
		ArrayList<Object> ret = new ArrayList<Object>();
		while (ss.getTop() != this)
			ret.add(ss.pop());
		Collections.reverse(ret);
		ss.pop();
		return ret;
	}
}
