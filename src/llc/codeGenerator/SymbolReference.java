package llc.codeGenerator;

import llc.codeGenerator.instruction.Type;
import llc.symbolTable.Symbol;

public class SymbolReference {
	Symbol symbol;

	public Symbol getSymbol() {
		return symbol;
	}

	public void setSymbol(Symbol symbol) {
		this.symbol = symbol;
		symbol.setAttr(this);
	}

	public SymbolReference(Symbol symbol) {
		setSymbol(symbol);
	}
	
	public boolean isGlobal(){
		return symbol.isGlobal();
	}
	
	public Type getType(){
		return symbol.getType();
	}
}
