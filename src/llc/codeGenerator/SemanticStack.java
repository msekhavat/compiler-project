package llc.codeGenerator;

import java.util.ArrayList;

import llc.codeGenerator.instruction.Operand;

public class SemanticStack extends ArrayList<Object>{
	private static final long serialVersionUID = 1483747724027591868L;

	public void push(Object element){
		add(element);
	}
	
	public Object pop(){
		Object ret = getTop();
		pop(1);
		return ret;
	}
	
	public void pop(int count){
		for (int i=0; i<count; i++)
			remove(size()-1);
	}
	
	public Object getTop(){
		return getTop(1);
	}
	
	public Object getTop(int count){
		if (size()==0)
			return null;
		return get(size()-count);
	}
	
	public Operand getOperandAndPop(){
		Operand res = (Operand) this.getTop();
		this.pop();
		return res;
	}
	
}