package llc.codeGenerator;

import java.util.ArrayList;

import llc.codeGenerator.instruction.Instruction;

public class GeneratedInstruction {

	private ArrayList<Instruction> instructions = null;
	private Integer mainAddress = new Integer(-1);

	public GeneratedInstruction(){
		instructions = new ArrayList<Instruction>();
	}

	public void addInstruction(Instruction ins){
		instructions.add(ins);
	}

	public void addInstruction(Instruction ins, int index){
		instructions.set(index, ins);
	}

	public Integer getMainAddress() {
		return mainAddress;
	}

	public void setMainAddress(Integer mainAddress) {
		this.mainAddress = mainAddress;
	}

	public int getTop() {
		return instructions.size();
	}

	public void increaseTop(){
		instructions.add(null);
	}
	
	public Instruction getIndex(int i){
		return instructions.get(i);
	}
	

}
