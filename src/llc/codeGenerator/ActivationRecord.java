package llc.codeGenerator;

import llc.codeGenerator.instruction.Type;

public class ActivationRecord {
	/**
	 * each ActivationRecord is created for a Function
	 * for global record, this should be null.
	 */
	private final Function function; 
	
	private int size;

	private int begin;
	
	public ActivationRecord(Function function, int begin){
		this.function = function;
		if (function != null)
			function.setActivationRecord(this);
		if (function == null)
			size = 12; //for function returning globals
		else
			size = 12; //for local functions (sp)
		this.setBegin(begin);
	}
	
	public int malloc(int numOfBytes){
		int ret = size;
		size += numOfBytes;
		return ret;
	}
	
	public int malloc(Type type){
		return malloc(type.getSize());
	}
	
	public int malloc(Variable variable){
		return malloc(variable.getSize());
	}
	
	public boolean isGlobal(){
		return function==null;
	}

	public int getSize() {
		return size;
	}

	public Function getFunction(){
		return function;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}
}
