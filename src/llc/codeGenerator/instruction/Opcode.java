package llc.codeGenerator.instruction;

public enum Opcode {
	ADD("+"), SUB("-"), MULT("*"), DIV("/"), MOD("%"), AND("&&"), OR("||"), LT("<"),
	GT(">"), LE("<="), GE(">="), EQ("=="), NEQ("!="), NOT("!"), MIN("u-"), ASSIGN(":="), 
	JZ("jz"), JMP("jmp"), WINT("wi"), WFLOAT("wf"), WTXT("wt"), RINT("ri"), RFLOAT("rf"),
	PCV(":=pc"), SPV(":=sp"), SPASSIGN("sp:="), RET("ret"), ALOC("aloc"), ERR("err");

	private String str;
	
	Opcode(String str){
		this.str = str;
	}
	
	public String toString(){
		return str;
	}
	
	public static Opcode getOpcode(String str){
		Opcode[] codes = Opcode.values();
		for(int i = 0 ; i < codes.length ; i++){
			if(codes[i].toString().equals(str)){
				return codes[i];
			}
		}
		return null;
	}
}
