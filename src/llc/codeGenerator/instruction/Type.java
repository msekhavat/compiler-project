package llc.codeGenerator.instruction;

public enum Type {
	INT("i_", 4), FLOAT("f_", 4), BOOL("b_", 1), VOID("v_", 0);//void for know the method is void
	
	private String str;
	private int size;
	
	Type(String str, int size){
		this.str = str;
		this.size = size;
	}
	
	public String toString(){
		return str;
	}
	
	public static Type getType(String str){
		Type[] types = Type.values();
		for(int i = 0 ; i < types.length ; i++){
			if(types[i].toString().equals(str)){
				return types[i];
			}
		}
		return null;
	}

	public int getSize() {
		return size;
	}
}
