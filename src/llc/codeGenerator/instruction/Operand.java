package llc.codeGenerator.instruction;

public class Operand {
	private Mod mod;
	private Type type;
	private String value;
	
	public Operand(Mod mod, Type type, Object value){
		this.mod = mod;
		this.type = type;
		setValue(value);
	}
	
	public Operand(int immediate){
		this(Mod.IM, Type.INT, immediate);
	}

	public Mod getMod() {
		return mod;
	}

	public Type getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
	
	public void setMod(Mod mod) {
		this.mod = mod;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setValue(Object value) {
		this.value = value.toString();
	}

	public String toString(){
		return "" + mod + type + value;
	}
}
