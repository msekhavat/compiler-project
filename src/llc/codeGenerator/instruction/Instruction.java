package llc.codeGenerator.instruction;

import java.util.ArrayList;
import java.util.Arrays;

public class Instruction {
	private Opcode opcode;
	private ArrayList<Operand> operands;
	

	public Instruction(Opcode opcode, Operand... operands){
		this.opcode = opcode;
		this.operands = new ArrayList<Operand>(Arrays.asList(operands));
	}
	
	public void addOperand(Operand op){
		operands.add(op);
	}
	
	public void setOperand(int index, Operand op){
		operands.set(index, op);
	}
	
	public Opcode getOpcode(){
		return opcode;
	}
	
	public Operand[] getOperands(){
		Operand[] result = new Operand[operands.size()];
		operands.toArray(result);
		return result;
	}
	
	public String toString(){
		String result = opcode.toString();
		for(int i = 0 ; i < operands.size() ; i++){
			result += " " + operands.get(i);
		}
		return result;
	}
}
