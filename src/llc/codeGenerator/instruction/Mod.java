package llc.codeGenerator.instruction;

public enum Mod {
	GD("gd_"), GI("gi_"), LD("ld_"), LI("li_"), IM("im_");

	private String str;
	
	Mod(String str){
		this.str = str;
	}
	
	public String toString(){
		return str;
	}
	
	public static Mod getMod(String str){
		Mod[] mods = Mod.values();
		for(int i = 0 ; i < mods.length ; i++){
			if(mods[i].toString().equals(str)){
				return mods[i];
			}
		}
		return null;
	}
}
