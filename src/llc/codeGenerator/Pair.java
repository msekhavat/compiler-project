package llc.codeGenerator;


public class Pair{
	private int continueAddr, breakAddr;
	
	public Pair(){
	}
	
//	public Pair(int c, int b){
//		this.setBreakAddr(b);
//		this.setContinueAddr(c);
//	}

	public int getContinueAddr() {
		return continueAddr;
	}

	public void setContinueAddr(int continueAddr) {
		this.continueAddr = continueAddr;
	}

	public int getBreakAddr() {
		return breakAddr;
	}

	public void setBreakAddr(int breakAddr) {
		this.breakAddr = breakAddr;
	}
}
