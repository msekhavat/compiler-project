package llc;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

import llc.codeGenerator.CodeGenerator;
import llc.lexicalAnalysis.LexicalAnalyzer;
import llc.parse.SLR1Parse;

/**
 * main class for compiler
 */
public class LLanguageCompiler {

	/**
	 * main method
	 * 
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {
		//generate grammar inputs:
		/**Process p = Runtime.getRuntime().exec("python generate.py");
		p.waitFor();
		if (p.exitValue()!=0)
			throw new Exception("could not generate input");
			/**/
		
		InputStreamReader reader;
		PrintStream writer;

	//	reader = new FileReader(args[0]);
		reader = new FileReader("Test.L");
	//	reader = new InputStreamReader(System.in);

		writer = new PrintStream(new BufferedOutputStream(new FileOutputStream("a.Lm")));
//		writer = System.out;

		LexicalAnalyzer lexical = new LexicalAnalyzer(reader);
		SLR1Parse slr1Parse = new SLR1Parse();
		CodeGenerator codeGen = new CodeGenerator(writer);
		slr1Parse.setLexical(lexical);
		slr1Parse.setCodeGen(codeGen);
		slr1Parse.parse();
		
		slr1Parse.getCodeGen().writeInstructionInOutputFile();
		writer.close();
	}

}
