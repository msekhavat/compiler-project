package llc.lexicalAnalysis;

abstract class CharGroup {
	abstract boolean has(char ch);

	public boolean equals(Object obj) {
		return has((Character) obj);
	}
}

public class CharUtils {
	public static final char EOF = (char) 26;
	public static CharGroup LETTER = new CharGroup() {
		boolean has(char ch) {
			return isLetter(ch);
		}
	}, DIGIT = new CharGroup() {
		boolean has(char ch) {
			return isDigit(ch);
		}

	}, ALL = new CharGroup() {
		boolean has(char ch) {
			return true;
		}

	}, WHITESPACE = new CharGroup() {
		boolean has(char ch) {
			return isWhiteSpace(ch);
		}
	};

	public static boolean isDigit(char ch) {
		if (ch >= '0' && ch <= '9')
			return true;
		return false;
	}

	public static boolean isLetter(char ch) {
		if (ch >= 'a' && ch <= 'z')
			return true;
		if (ch >= 'A' && ch <= 'Z')
			return true;
		return false;
	}

	public static boolean isWhiteSpace(char ch) {
		if (ch == ' ' || ch == '\t' || ch == '\n')
			return true;
		return false;
	}

}
