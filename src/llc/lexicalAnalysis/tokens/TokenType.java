package llc.lexicalAnalysis.tokens;

public enum TokenType {
	OPEN_BRACE("{"), CLOSE_BRACE("}"), OPEN_BRACKET("["), CLOSE_BRACKET("]"), OPEN_PARENT("("), CLOSE_PAREN(
			")"), COMMA(","), SEMICOLON(";"), SET("="), PLUS("+"), MINUS("-"), ASTERISK("*"), SLASH(
			"/"), PERCENT("%"), NOT("!"), LT("<"), GT(">"), LE("<="), GE(">="), EQ("=="), NE("!="), AND(
			"&&"), OR("||"), ID("id"), KEYWORD, INT_NUM("int_num"), FLOAT_NUM("float_num"), EOF;
	private String repr;

	private TokenType(String repr) {
		this.repr = repr;
	}

	private TokenType() {
		this(null);
	}

	public String toString() {
		return (repr == null ? this.name() : repr);
	}
}
