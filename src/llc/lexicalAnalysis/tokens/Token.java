package llc.lexicalAnalysis.tokens;

import llc.lexicalAnalysis.scanner.Position;

public class Token {
	protected TokenType type;
	protected Position position;
	protected Object attr;

	public Token(TokenType type, Position position, Object attr) {
		super();
		this.type = type;
		this.position = position;
		this.attr = attr;
	}

	public Token(TokenType type, Position position) {
		this(type, position, null);
	}

	public TokenType getType() {
		return type;
	}

	public Position getPosition() {
		return position;
	}

	public Object getAttr() {
		return attr;
	}

	public void setAttr(Object attr) {
		this.attr = attr;
	}

	@Override
	public String toString() {
		return type.toString();
	}

}
