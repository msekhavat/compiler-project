package llc.lexicalAnalysis.tokens;

import llc.lexicalAnalysis.scanner.Position;

public class TokenKeyword extends Token {

	public TokenKeyword(Position position, Keyword keyword) {
		super(TokenType.KEYWORD, position, keyword);
	}

	public Keyword getKeyword() {
		return (Keyword) attr;
	}

	@Override
	public String toString() {
		return getKeyword().getLexeme();
	}
}
