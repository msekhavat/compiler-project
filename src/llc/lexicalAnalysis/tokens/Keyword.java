package llc.lexicalAnalysis.tokens;


public enum Keyword {
	BOOL("bool"), INT("int"), FLOAT("float"), FOR("for"), BREAK("break"), CONTINUE("continue"), IF(
			"if"), ELSE("else"), FALSE("false"), TRUE("true"), READFLOAT("readfloat"), READINT(
			"readint"), RETURN("return"), VOID("void"), WRITEFLOAT("writefloat"), WRITEINT(
			"writeint");

	private final String lexeme;

	Keyword(String lexeme) {
		this.lexeme = lexeme;
	}

	public static Keyword fromLexeme(String lexeme) {
		for (Keyword keyword : Keyword.values()) {
			if (keyword.lexeme.equals(lexeme))
				return keyword;
		}
		return null;
	}

	public String getLexeme() {
		return lexeme;
	}
}
