package llc.lexicalAnalysis.tokens;

import llc.lexicalAnalysis.scanner.Position;

public class TokenId extends Token {

	public TokenId(Position position, String lexeme) {
		super(TokenType.ID, position, lexeme);
	}

	public String getLexeme(){
		return (String) attr;
	}
}
