package llc.lexicalAnalysis;

import java.io.IOException;
import java.io.InputStreamReader;

import llc.exceptions.ImplementationException;
import llc.exceptions.InvalidTokenException;
import llc.lexicalAnalysis.scanner.Position;
import llc.lexicalAnalysis.scanner.Scanner;
import llc.lexicalAnalysis.tokens.Keyword;
import llc.lexicalAnalysis.tokens.Token;
import llc.lexicalAnalysis.tokens.TokenId;
import llc.lexicalAnalysis.tokens.TokenKeyword;
import llc.lexicalAnalysis.tokens.TokenType;
import llc.symbolTable.SymbolTable;

/**
 * This class should tokenize the input which scanner reads char by char.
 */

public class LexicalAnalyzer {
	private SymbolTable symbolTable = new SymbolTable();
	Scanner scanner;
	char current;
	private State state;

	public LexicalAnalyzer(InputStreamReader input) {
		scanner = new Scanner(input);
	}

	public Position getCurrentPosition(){
		return scanner.getPosition();
	}
	
	private static class AcceptToken extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1385024217237487277L;
		private Token token;

		public AcceptToken(Token t) {
			this.token = t;
		}

		public Token getToken() {
			return token;
		}
	}

	private static class StateTransition extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4205308801874709524L;
		private State nextState;

		public StateTransition(State nexState) {
			this.nextState = nexState;
		}

		public State getNextState() {
			return nextState;
		}
	}

	private enum Action {
		RETRACT, RESTART_LEXEME;

	}

	private void runActions(Action... actions) {
		for (Action action : actions) {
			switch (action) {
			case RETRACT:
				scanner.retractChar();
				break;
			case RESTART_LEXEME:
				scanner.startLexeme();
				break;
			}

		}
	}

	private void handle(Object inputSelector, Object resultType, Action... actions)
			throws AcceptToken, StateTransition, ImplementationException {
		if (!inputSelector.equals(current))
			return;

		runActions(actions);

		if (resultType instanceof State)
			throw new StateTransition((State) resultType);

		if (resultType instanceof TokenType) {
			resultType = (TokenType)resultType;
			if( resultType == TokenType.INT_NUM || resultType == TokenType.FLOAT_NUM )
				throw new AcceptToken(new Token((TokenType) resultType, scanner.getLexemePosition(), scanner.getLexeme()));//we want lexeme
			throw new AcceptToken(new Token((TokenType) resultType, scanner.getLexemePosition()));
		}

		throw new ImplementationException("unknown `resultType`");

	}

	private enum State {
		BEGIN, END, SLASH, DIGIT, DOT, COMMENT, COMMENT_MULTILINE, COMMENT_MULTILINE_STAR, DOT_DIGIT, LETTER, DIGIT_DOT, LT, GT, EQ, NOT, AND, OR;

	}

	public Token nextToken() throws InvalidTokenException, IOException, ImplementationException {
		state = State.BEGIN;
		scanner.startLexeme();
		try {
			do {
				current = scanner.getNextChar();
				// System.err.println("state=" + state + " lexeme='" +
				// scanner.getLexeme() + "' current='" + current+"'");
				try {
					CharGroup etc = CharUtils.ALL;
					switch (state) {
					case BEGIN:
						handle(CharUtils.WHITESPACE, State.BEGIN, Action.RESTART_LEXEME);
						handle(CharUtils.LETTER, State.LETTER);
						handle(CharUtils.DIGIT, State.DIGIT);
						handle('<', State.LT);
						handle('>', State.GT);
						handle('=', State.EQ);
						handle('!', State.NOT);
						handle('&', State.AND);
						handle('|', State.OR);
						handle('/', State.SLASH);
						handle('.', State.DOT);
						handle('*', TokenType.ASTERISK);
						handle('+', TokenType.PLUS);
						handle('-', TokenType.MINUS);
						handle('%', TokenType.PERCENT);
						handle('{', TokenType.OPEN_BRACE);
						handle('}', TokenType.CLOSE_BRACE);
						handle('[', TokenType.OPEN_BRACKET);
						handle(']', TokenType.CLOSE_BRACKET);
						handle('(', TokenType.OPEN_PARENT);
						handle(')', TokenType.CLOSE_PAREN);
						handle(';', TokenType.SEMICOLON);
						handle(',', TokenType.COMMA);
						handle(CharUtils.EOF, TokenType.EOF);
						break;
					case LT:
						handle('=', TokenType.LE);
						handle(etc, TokenType.LT, Action.RETRACT);
						break;
					case GT:
						handle('=', TokenType.GE);
						handle(etc, TokenType.GT, Action.RETRACT);
						break;
					case EQ:
						handle('=', TokenType.EQ);
						handle(etc, TokenType.SET, Action.RETRACT);
						break;
					case NOT:
						handle('=', TokenType.NE);//! => = a BUG FIXED
						handle(etc, TokenType.NOT, Action.RETRACT);
						break;
					case AND:
						handle('&', TokenType.AND);
						break;
					case OR:
						handle('|', TokenType.OR);
						break;
					case SLASH:
						handle('/', State.COMMENT);
						handle('*', State.COMMENT_MULTILINE);
						handle(etc, TokenType.SLASH, Action.RETRACT);
						break;
					case COMMENT:
						handle('\n', State.BEGIN, Action.RESTART_LEXEME);
						handle(etc, State.COMMENT);
						break;
					case COMMENT_MULTILINE:
						handle('*', State.COMMENT_MULTILINE_STAR);
						handle(etc, State.COMMENT_MULTILINE);
						break;
					case COMMENT_MULTILINE_STAR:
						handle('/', State.BEGIN, Action.RESTART_LEXEME);
						handle('*', State.COMMENT_MULTILINE_STAR);
						handle(etc, State.COMMENT_MULTILINE);
						break;
					case LETTER:
						handle(CharUtils.LETTER, State.LETTER);
						handle(CharUtils.DIGIT, State.LETTER);
						handle('_', State.LETTER);
						keywordOrId();
						break;
					case DIGIT:
						handle(CharUtils.DIGIT, State.DIGIT);
						handle('.', State.DIGIT_DOT);
						handle(etc, TokenType.INT_NUM, Action.RETRACT);
						break;
					case DIGIT_DOT:
						handle(CharUtils.DIGIT, State.DIGIT_DOT);
						handle(etc, TokenType.FLOAT_NUM, Action.RETRACT);
						break;
					case DOT:
						handle(CharUtils.DIGIT, State.DIGIT_DOT);
						break;
					default:
						throw new ImplementationException("Unhandled state " + state);
					}
					throw new InvalidTokenException(scanner.getLexemePosition(),
							scanner.getLexeme());
				} catch (StateTransition e) {
					state = e.getNextState();
				}
			} while (current != CharUtils.EOF);
			//unfinished token
			throw new InvalidTokenException(scanner.getLexemePosition(), scanner.getLexeme());
		} catch (AcceptToken e) {
			return e.getToken();
		}
	}

	private void keywordOrId() throws AcceptToken {
		runActions(Action.RETRACT);
		String lexeme = scanner.getLexeme();
		Keyword keyword = Keyword.fromLexeme(lexeme);
		Token token;
		if (keyword == null)
			token = new TokenId(scanner.getLexemePosition(), lexeme);
		else
			token = new TokenKeyword(scanner.getLexemePosition(), keyword);
		throw new AcceptToken(token);
	}
	
	public String getCurrentLexeme(){
		return scanner.getLexeme();
	}

	public SymbolTable getSymbolTable() {
		return symbolTable;
	}
	
}
