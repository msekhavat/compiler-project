package llc.lexicalAnalysis.scanner;


/**
 * Stores position of a character or beginning of a token. FUTURE ENHANCEMENT:
 * add `filename` property
 */
public class Position implements Cloneable {
	private int lineNumber;
	private int charNumber;

	public Position(int lineNumber, int charNumber) {
		super();
		this.lineNumber = lineNumber;
		this.charNumber = charNumber;
	}

	public Position() {
		this(0, 0);
	}

	public int getLineNumber() {
		return lineNumber+1;
	}

	public int getCharNumber() {
		return charNumber+1;
	}

	public Object clone() {
		return new Position(lineNumber, charNumber);
	}

	public int forward(char current, int lastLineCharNumber) {
		if (current == '\n') {
			lastLineCharNumber = charNumber;
			lineNumber++;
			charNumber = 0;
		} else
			charNumber++;
		return lastLineCharNumber;
	}

	public void backward(char current, int lastLineCharNumber) {
		if (current == '\n') {
			lineNumber--;
			charNumber = lastLineCharNumber;
		} else {
			charNumber--;
		}
	}
	
	public String toString(){
		return "(line "+getLineNumber()+" col "+getCharNumber()+")";
	}
}