package llc.lexicalAnalysis.scanner;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Keeps track of position of buffer
 * 
 * @author mohammad
 */
public class Scanner {
	private BufferReader bufferReader;
	private Position currentPosition, lexemePosition;
	private int lastLineCharNumber;
	private char current;

	public Scanner(InputStreamReader input) {
		super();
		this.bufferReader = new BufferReader(input);
		lexemePosition = currentPosition = new Position(0, 0);
	}

	public Position getPosition() {
		return currentPosition;
	}

	public void retractChar() {
		//Only storing 1 last line's charNumber. breaks on two times retracting '\n'.
		currentPosition.backward(current, lastLineCharNumber);
		bufferReader.retract();
	}

	public char getNextChar() throws IOException {
		current = bufferReader.nextChar();
		lastLineCharNumber = currentPosition.forward(current, lastLineCharNumber);
		return current;
	}

	public String getLexeme() {
		return bufferReader.getLexeme();
	}

	public void startLexeme() {
		bufferReader.startLexeme();
		lexemePosition = (Position) currentPosition.clone();
	}

	public Position getLexemePosition() {
		return lexemePosition;
	}

}