package llc.lexicalAnalysis.scanner;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class handles reading input from file/stdin and buffering
 */
public class BufferReader {
	public static final char EOF = (char) 26;
	private static final int BUFFER_SIZE = 1024;
	private InputStreamReader input;

	private char[][] buffer = new char[2][BUFFER_SIZE];
	private int lexemeBegin, lexemeForward;
	private int countRetracts = 0;
	private int eofLimit = BUFFER_SIZE;

	public BufferReader(InputStreamReader input) {
		this.input = input;
		lexemeBegin = 0;
		lexemeForward = -1;
	}

	/**
	 * reads next character from input. returns Scanner.EOF if reached end of
	 * file
	 * 
	 * @return
	 * @throws IOException
	 */
	public char nextChar() throws IOException {
		lexemeForward = (lexemeForward + 1) % (2 * BUFFER_SIZE);
		if (countRetracts > 0)
			countRetracts--;

		int bufferPosition = lexemeForward % BUFFER_SIZE;
		int bufferNumber = lexemeForward / BUFFER_SIZE;
		if (bufferPosition == 0 && countRetracts == 0)
			eofLimit = input.read(buffer[bufferNumber]);

		if (bufferPosition >= eofLimit) {
			eofLimit = -1;
			return EOF;
		}

		return buffer[bufferNumber][bufferPosition];
	}

	/**
	 * puts read characters back to input.
	 * 
	 * @param count
	 *            : number of chars to put back
	 */
	private void retract(int count) {
		lexemeForward = (lexemeForward + BUFFER_SIZE - count) % BUFFER_SIZE;
	}

	public void retract() {
		retract(1);
	}

	/**
	 * @return a String containing current lexeme.
	 */
	public String getLexeme() {
		String ret = "";
		int n = BUFFER_SIZE * 2;
		for (int i = lexemeBegin; (i + n - 1) % n != lexemeForward; i = (i + 1) % n)
			ret += buffer[i / BUFFER_SIZE][i % BUFFER_SIZE];
		return ret;
	}

	/**
	 * Starts new lexeme and skips currently read chars. Next getLexeme() call
	 * won't include chars before current position of 'forward' pointer.
	 */
	public void startLexeme() {
		lexemeBegin = (lexemeForward + 1) % (2 * BUFFER_SIZE);
	}

}
