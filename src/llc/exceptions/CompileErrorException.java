package llc.exceptions;

import llc.lexicalAnalysis.scanner.Position;

public class CompileErrorException extends InputErrorException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CompileErrorException(Position position) {
		super(position);
	}

	public CompileErrorException(Position position, String description){
		super(position, description);
	}
}
