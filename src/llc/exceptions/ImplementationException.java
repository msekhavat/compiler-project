package llc.exceptions;

public class ImplementationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2676368706478354531L;

	public ImplementationException(String string) {
		super(string);
	}

}
