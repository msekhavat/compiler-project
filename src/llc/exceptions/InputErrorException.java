package llc.exceptions;

import llc.lexicalAnalysis.scanner.Position;

public class InputErrorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6303737067951488126L;
	protected Position position;

	public InputErrorException(Position position, String message) {
		super(message);
		this.position = position;
	}
	
	public InputErrorException(Position position){
		this(position, null);
	}

	public int getLineNumber() {
		return position.getLineNumber();
	}

	public int getCharNumber() {
		return position.getCharNumber();
	}

	public Position getPosition(){
		return position;
	}

	@Override
	public String toString() {
		String ret = "Error at line " + getLineNumber() + " col " + getCharNumber();
		if (getMessage() != null)
			ret += ": " + getMessage();
		return ret;
	}
}