package llc.exceptions;

import llc.lexicalAnalysis.scanner.Position;

/**
 * this exception occurs in Lexical Analysis phase. (by Scanner Class)
 */
public class InvalidTokenException extends InputErrorException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3055965417619067087L;
	private String invalidToken;

	public InvalidTokenException(Position position, String invalidToken) {
		super(position);
		this.invalidToken = invalidToken;
	}

	public String getInvalidToken() {
		return invalidToken;
	}
}
