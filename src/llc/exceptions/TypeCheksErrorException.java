package llc.exceptions;

import llc.lexicalAnalysis.scanner.Position;

public class TypeCheksErrorException extends CompileErrorException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5909224787987905772L;

	public TypeCheksErrorException(Position position) {
		super(position);
	}

}
