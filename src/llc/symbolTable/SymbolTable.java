package llc.symbolTable;

import llc.codeGenerator.instruction.Type;
import llc.exceptions.ImplementationException;
import llc.lexicalAnalysis.scanner.Position;

public class SymbolTable {
	Scope activeScope = new Scope();
	
	public void setDefiningNewSymbol(Type top){
		activeScope.setDefiningNewSymbol(top);
	}
	
	public Symbol getSymbol(String lexeme, Position position) throws SymbolExistanceException, ImplementationException {
		return activeScope.getSymbol(lexeme, position);
	}
	
	public Scope openNewScope(){
		Scope ret = new Scope();
		ret.setOuterScope(activeScope);
		activeScope = ret;
		return ret;
	}
	
	public void closeLastScope(){
		activeScope = activeScope.getOuterScope();
	}

}
