package llc.symbolTable;

import llc.exceptions.CompileErrorException;
import llc.lexicalAnalysis.scanner.Position;

public class SymbolExistanceException extends CompileErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3675021507954538074L;
	private String lexeme;
	public SymbolExistanceException(Position position, String lexeme, boolean alreadyExists) {
		super(position);
		this.lexeme = lexeme;
	}

	public String toString(){
		return "SymbolExistanceException(" + lexeme + ") at " + position; 
	}
}
