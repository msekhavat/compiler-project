package llc.symbolTable;

import java.util.HashMap;
import java.util.Map;

import llc.codeGenerator.instruction.Type;
import llc.exceptions.ImplementationException;
import llc.lexicalAnalysis.scanner.Position;

public class Scope {
	private Type typeOfDefiningNewSymbol;
	private Scope outerScope = null;
	
	Map<String, Symbol> dict = new HashMap<String, Symbol>();

	public Symbol getSymbol(String lexeme, Position position) throws SymbolExistanceException, ImplementationException {
		Symbol symbol = dict.get(lexeme);
		if (isDefiningNewSymbol()){
			if (symbol != null)
				throw new SymbolExistanceException(position, lexeme, true);
			if( typeOfDefiningNewSymbol == null )
				throw new ImplementationException("should set type befor defining new type");
			
			symbol = new Symbol(lexeme, position, this, typeOfDefiningNewSymbol);
			dict.put(lexeme, symbol);
			
			//automatically turn off isDefiningNewSymbol and type to null and decl
			setDefiningNewSymbol(null);
		} else if (symbol == null){
			if (outerScope == null)
				throw new SymbolExistanceException(position, lexeme, false);
			symbol = outerScope.getSymbol(lexeme, position);
		}
		return symbol;
	}

	public boolean isDefiningNewSymbol() {
		return typeOfDefiningNewSymbol != null;
	}

	public void setDefiningNewSymbol(Type top) {
		this.typeOfDefiningNewSymbol = top;
	}

	public Scope getOuterScope() {
		return outerScope;
	}

	public void setOuterScope(Scope outerScope) {
		this.outerScope = outerScope;
	}

	public boolean isGlobal(){
		return outerScope == null;
	}
	
}
