package llc.symbolTable;

import llc.codeGenerator.SymbolReference;
import llc.codeGenerator.instruction.Type;
import llc.lexicalAnalysis.scanner.Position;

public class Symbol {
	private String lexeme;
	private Scope scope;
	private Type type;
	private SymbolReference attr;
	private Position positionOfDefinition;
	
	public Symbol(){
		
	}
	
	public Symbol(String lexeme, Position position, Scope scope, Type type) {
		this.scope = scope;
		this.lexeme = lexeme;
		this.type = type;
		this.positionOfDefinition = position;
	}
	
	public Symbol(Symbol symbol){
		this.lexeme = symbol.lexeme;
		this.type = symbol.type;
		this.scope = symbol.scope;
	}
	
	public String getLexeme() {
		return lexeme;
	}

	public void setLexeme(String lexeme) {
		this.lexeme = lexeme;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Boolean isGlobal() {
		return scope.isGlobal();
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Object getAttr() {
		return attr;
	}

	public void setAttr(SymbolReference attr) {
		this.attr = attr;
	}

	@Override
	public String toString() {
		return getLexeme() + " in " + scope;
	}

	public Position getPositionOfDefinition() {
		return positionOfDefinition;
	}

}
