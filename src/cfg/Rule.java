package cfg;

import java.util.ArrayList;

public class Rule {
	private Token LHS;
	private ArrayList<Token> RHS;
	private int num;
	
	public Rule(Token LHS, ArrayList<Token> RHS, int num) {
		this.LHS = LHS;
		this.RHS = RHS;
		this.num = num;
	}

	public int getNum(){
		return num;
	}
	
	public Token getLHS() {
		return LHS;
	}

	public ArrayList<Token> getRHS() {
		return RHS;
	}
	
	public Token getToken(int index){
		return RHS.get(index);
	}
	
	public int getSize(){
		return RHS.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((LHS == null) ? 0 : LHS.hashCode());
		result = prime * result + ((RHS == null) ? 0 : RHS.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rule other = (Rule) obj;
		if (LHS == null) {
			if (other.LHS != null)
				return false;
		} else if (!LHS.equals(other.LHS))
			return false;
		if (RHS == null) {
			if (other.RHS != null)
				return false;
		} else if (!RHS.equals(other.RHS))
			return false;
		return true;
	}

	public String toString() {
		String ret = LHS+" : ";
		for (Token token : RHS){
			ret += token + " ";
		}
		return ret;
	}

	
}
