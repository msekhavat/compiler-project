package cfg;

import java.util.ArrayList;
import java.util.HashMap;

public class CFG {
	private Token start;
	private ArrayList<Rule> rules;
	
	private HashMap<Token, ArrayList<Rule>> tokenRules;
	private ArrayList<Token> tokens;
	
	private HashMap<Token, ArrayList<Token>> first;
	private HashMap<Token, ArrayList<Token>> follow;
	
	private HashMap<Token, Boolean> nullable;
	
	public CFG(Token start, ArrayList<Rule> rules){
		this.start = start;
		this.rules = (rules == null ? new ArrayList<Rule>() : rules);
		
		//Adding the additional start rule
		ArrayList<Token> startRuleToks = new ArrayList<Token>();
		startRuleToks.add(start);
		startRuleToks.add(new Token("$", TokenType.TERMINAL));
		this.rules.add(0, new Rule(new Token("", TokenType.START), startRuleToks, 0));
		
		tokenRules = new HashMap<Token, ArrayList<Rule>>();
		first = new HashMap<Token, ArrayList<Token>>();
		follow = new HashMap<Token, ArrayList<Token>>();
		nullable = new HashMap<Token, Boolean>();
	}
	
	public ArrayList<Rule> getTokenRules(Token t){
		ArrayList<Rule> result = tokenRules.get(t);
		if(result == null){
			result = new ArrayList<Rule>();
			for(Rule r : rules){
				if(r.getLHS().equals((t))){
					result.add(r);
				}
			}
			tokenRules.put(t, result);
		}
		return result;
	}
	
	private ArrayList<Rule> getRHSRules(Token t){
		ArrayList<Rule> result = new ArrayList<Rule>();
		for(Rule rule : rules){
			if(rule.getRHS().contains(t) && !result.contains(rule)){
				result.add(rule);
			}
		}
		return result;
	}

	public ArrayList<Token> follow(Token t){
//		System.out.println("follow " + t);
		ArrayList<Token> result = follow.get(t);
		if(result != null){
			return result;
		}
		if(t.getType() == TokenType.TERMINAL){
			follow.put(t, new ArrayList<Token>());
			return new ArrayList<Token>();
		}
		result = computeFollow(t, new ArrayList<Token>());
		follow.put(t, result);
		return result;
	}
	
	private ArrayList<Token> computeFollow(Token t, ArrayList<Token> path){
//		System.out.println("computefollow " + t);
		ArrayList<Token> result = follow.get(t);
		if(result != null){
			return result;
		}
		result = new ArrayList<Token>();
		path.add(t);
//		System.out.println("path " + path);
		ArrayList<Rule> rhsRules = getRHSRules(t);
		for(Rule rule : rhsRules){
			ArrayList<Token> rhs = new ArrayList<Token>(rule.getRHS());
			int index = rhs.indexOf(t);
			while(index >= 0){
				if(index == rhs.size() - 1 && !rule.getLHS().equals(t) && !path.contains(rule.getLHS())){
					result = addAll(result, computeFollow(rule.getLHS(), path));
					break;
				}
				for(int i = index + 1 ; i < rhs.size() ; i++){
					Token tok = rhs.get(i);
					if(tok.getType() == TokenType.TERMINAL){
						if(!result.contains(tok)){
							result.add(tok);
						}
						break;
					}
//					else if(!path.contains(tok)){
						result = addAll(result, first(tok));
//					}
					if(!nullable(tok)){
						break;
					}
					if(i == rhs.size() - 1 && !rule.getLHS().equals(t) && !path.contains(rule.getLHS())){
						result = addAll(result, computeFollow(rule.getLHS(), path));
					}
				}
				rhs.set(index, null);
				index = rhs.indexOf(t);
			}
		}
		path.remove(path.size() - 1);
		return result;
	}
	
	
	public ArrayList<Token> first(Token t){
//		System.out.println("first " + t);
		ArrayList<Token> result = first.get(t);
		if(result != null){
			return result;
		}
		if(t.getType() == TokenType.TERMINAL){
			first.put(t, new ArrayList<Token>());
			return new ArrayList<Token>();
		}
		result = computeFirst(t, new ArrayList<Token>());
		first.put(t, result);
		return result;
	}
	
	private ArrayList<Token> computeFirst(Token t, ArrayList<Token> path){
//		System.out.println("computerFirst " + t);
		ArrayList<Token> result = first.get(t);
		if(result != null){
			return result;
		}
		path.add(t);
		result = new ArrayList<Token>();
		ArrayList<Rule> tRules = getTokenRules(t);
		for(Rule rule : tRules){
			ArrayList<Token> rhs = rule.getRHS();
			for(int i = 0 ; i < rhs.size() ; i++){
				Token tok = rhs.get(i);
				if(tok.getType() == TokenType.TERMINAL){
					if(!result.contains(tok)){
						result.add(tok);
					}
					break;
				}
				else{
					if(!path.contains(tok)){
						result = addAll(result, computeFirst(tok, path));
					}
					if(!nullable(tok)){
						break;
					}
				}
			}
		}
		path.remove(path.size() - 1);
		return result;
	}
	
	public boolean nullable(Token t){
//		System.out.println("nullable " + t);
		Boolean result = nullable.get(t);
		if(result != null){
			return result;
		}
		result = computeNullable(t, new ArrayList<Token>());
		nullable.put(t, result);
		return result;
	}
	
	private boolean computeNullable(Token t, ArrayList<Token> path){
//		System.out.println("computerNullable " + t);
		Boolean result = nullable.get(t);
		if(result != null){
			return result;
		}
		
		if(t.getType() == TokenType.TERMINAL){
			return false;
		}
		
		path.add(t);
		ArrayList<Rule> rules = getTokenRules(t);
		for(Rule rule : rules){
			
			if(rule.getSize() == 0){
				nullable.put(t, true);
				path.remove(path.size() - 1);
				return true;
			}
			
			ArrayList<Token> ruleRHS = rule.getRHS();
			for(Token rt : ruleRHS){
				if(path.contains(rt)){
					break;
				}
				if(!computeNullable(rt, path)){
					break;
				}
				nullable.put(t, true);
				path.remove(path.size() - 1);
				return true;
			}
		}
		nullable.put(t, false);
		path.remove(path.size() - 1);
		return false;
		
	}
	
	public Token getStart(){
		return start;
	}
	
	public Rule getRule(int i){
		return rules.get(i);
	}
	
	public int getRulesSize(){
		return rules.size();
	}
	
	private ArrayList<Token> addAll(ArrayList<Token> list, ArrayList<Token> add){
		for(Token t : add){
			if(!list.contains(t)){
				list.add(t);
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return "CFG [start=" + start + ", rules=" + rules + ", tokenRules="
				+ tokenRules + "]";
	}
	
	public ArrayList<Token> getTokens(){
		if(tokens != null){
			return tokens;
		}
		tokens = new ArrayList<Token>();
		for(Rule r : rules){
			Token LHS = r.getLHS();
			if(!tokens.contains(LHS)){
				tokens.add(LHS);
			}
			for(Token t : r.getRHS()){
				if(!tokens.contains(t)){
					tokens.add(t);
				}
			}
		}
		tokens.remove(0); //:-"""
		return tokens;
	}
}
