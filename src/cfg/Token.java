package cfg;

public class Token {
	private String name;
	private TokenType type;
	
	public Token(String name, TokenType type){
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public TokenType getType() {
		return type;
	}
	
	public boolean isTerminal(){
		return type == TokenType.TERMINAL;
	}
	
	public boolean isVariable(){
		return type == TokenType.VAR;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
}
