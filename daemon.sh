while true; do
  change=$(inotifywait -e close_write,moved_to,create .)
  change=${change#./ * }
  if [ "$change" = "grammar.txt" ]; then ./generator.py; fi
done
