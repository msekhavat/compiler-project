#!/usr/bin/python
from collections import defaultdict
import re

rules = []
numbered_rules = {}

class Rule(tuple):
    def __new__(cls, lhs, rhs):
        global numbered_rules
        rhs = tuple(rhs)
        check = (lhs, rhs)
        if check in numbered_rules:
            return numbered_rules[check]
            
        ret = super(Rule, cls).__new__(cls, rhs)
        ret.lhs = lhs
        ret.number = len(rules)
        rules.append(ret)
        numbered_rules[check]=ret;
        return ret

    def __unicode__(self):
        return '{} : {}'.format(self.lhs, ' '.join(self))

    def __repr__(self):
        return self.__unicode__()

def add_rule(lhs, rhs):
    for i in range(len(rhs)):
        item = rhs[i]
        if item.startswith('%') and item != '%':
            item = rhs[i][1:]
            action_name = 'ACTION_' + item
            rhs[i] = '#' + action_name
            Rule(action_name, [])
            
    return Rule(lhs, rhs)

with open('grammar.txt', 'r') as grammar_file:
    main = ''
    while main.startswith('//') or main == '':
        main = grammar_file.readline().strip()
    for line in grammar_file:
        line = re.sub('//.*', '', line).strip()
        if (not ':' in line):
            continue
        lhs, rhs = line.split(':')
        lhs = lhs.strip()
        rhs = [token.strip() for token in rhs.split()]
        add_rule(lhs, rhs)

with open('input.txt', 'wb') as rules_file:
    rules_file.write('{}\n'.format(main))
    sorted_rules = sorted(numbered_rules.values(), key=lambda rule:rule.number)
    for rule in sorted_rules:
        rules_file.write('%s\n' % unicode(rule))

with open('actions.txt', 'wb') as actions_file:
    actions = ['%d %s' % (rule.number+1, rule.lhs[7:]) for rule in rules if rule.lhs.startswith('ACTION_')]
    actions_file.write('{}\n{}\n'.format(len(actions), '\n'.join(actions)))

print 'generated!'
